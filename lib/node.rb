module Jsdoc
  class Node
    attr_reader :name, :daddy, :attributes

    def initialize(name: nil, daddy: nil, attributes: {}, parent: nil)
      @name = name || attributes['name']
      @parent = parent
      @daddy = nil
      @childrens = []
      @attributes = attributes
      # debug "Defines #{@name} with attr=#{@attributes}"
      debug "Defines #{@name}"
    end

    def to_s
      "[Node:#{@name}:#{self.class}]"
    end

    # @return {true | false}
    def child?(name)
      debug "#{@name}.child?(#{name}) => #{@childrens.any? { |c| c.name == name }}"
      @childrens.any? { |c| c.name == name }
    end

    # get a child node by its name
    # @return {Node | nil}
    def child(name)
      @childrens.find { |c| c.name == name }
    end

    # add a new node
    def <<(node)
      @childrens << node
      node.attach(self)
      debug "Add #{node.name} into #{self.name}"
      self
    end

    # attach the node to the parent
    def attach(parent)
      @parent = parent
    end

    # remove a child
    def remove(name)
      @childrens.reject! { |child| child.name == name }
      self
    end

    # replace self in the parent and childs
    def redefine(node)
      @parent.remove(@name) << node if @parent
      @childrens.each { |c| c.attach(node) }
      node
    end

    # @returns {Node}
    def goto(full_path)
      path.reduce(@daddy) { |base, path| base.child(path) }
    end

    # return the full path with namespaces
    def get_fullname(joiner = '.', current = nil)
      updated = [self.name, current].compact.join(joiner)
      # puts "get_fullname(#{joiner}, #{current}) @parent=#{@parent}"
      return updated if !@parent
      return updated if @parent == self
      return @parent.get_fullname joiner, updated
    end

    def parent_amount(acc = 0)
      return acc if !@parent
      return acc if @parent == self
      return @parent.parent_amount acc + 1
    end

    def debug(*params)
      # params.each do |param|
      #   STDERR.puts "#{(' ' * 4 * parent_amount)} DEBUG [#{get_fullname}] #{param}"
      # end
    end

    def add_data_node data_node
      full_path = (data_node['memberof'] || "").split "."
      debug "Need to access #{full_path}"
      leaf = full_path.reduce(self) do |current_node, next_path|
        current_node.debug "current_node is #{current_node.name}, accessing #{next_path}"
        if !current_node.child?(next_path)
          current_node.debug "Generate empty namespace name=#{next_path} into #{current_node.name}"
          namespace = Jsdoc::Namespace.new(name: next_path, daddy: self.daddy, parent: current_node)
          current_node << namespace
          namespace
        end
        current_node.debug ">>>> NEXT LEAF: #{current_node.name}.child(#{next_path}) #{current_node.child(next_path).name}"
        current_node.child(next_path)
      end

      leaf.debug "leaf reach"
      klass = NODE_TYPES[data_node["kind"]] || NODE_TYPES["default"]
      node = klass.new(attributes: data_node, daddy: self.daddy, parent: leaf)

      if leaf.child? node.name
        leaf.debug "redefine #{node.name}"
        leaf.debug "children amount=#{leaf.childrens.size}"
        leaf.child(node.name).redefine(node)
        leaf.debug "children amount=#{leaf.childrens.size}"
      else
        leaf.debug "add #{node.name}"
        leaf << node
      end

      self
    end
  end

  class Namespace < Jsdoc::Node
  end

  class Klass < Jsdoc::Node
    def properties
      @attributes["properties"]
    end
  end

  class Function < Jsdoc::Node
    def params
      @attributes["params"]
    end
  end


  NODE_TYPES = {
    "class" => Jsdoc::Klass,
    "function" => Jsdoc::Function,
    "namespace" => Jsdoc::Function,
    "default" => Jsdoc::Node,
  }
end

require './lib/node/generator.rb'
