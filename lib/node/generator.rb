require 'slim'
require 'slim/include'

class SlimRender
  def include_slim(name, params, &block)
    old_params = @params
    @params = params || @params
    output_html = Slim::Template.new("lib/node/#{name}.slim", {}).render(self, &block)
    @params = old_params
    output_html
  end

  def [](name)
    @params[name]
  end

  def missing_method(name)
    self[name]
  end
end

module Jsdoc
  class Node
    def generate_html_tree
      self.generate_self_html
      @childrens.each(&:generate_html_tree)
    end

    def link
      'page' + self.get_fullname('.') + ".html"
    end

    def get_file(with_lines: false)
      return '' if !@attributes['meta']
      suffix = with_lines &&  @attributes['meta']['range'] ? ":#{@attributes['meta']['range'][0]}:#{@attributes['meta']['range'][1]}" : ''
      "#{@attributes['meta']['path']}/#{@attributes['meta']['filename']}#{suffix}"
    end

    def get_all_parents_ordered(parents = [])
      return @parent.get_all_parents_ordered [@parent] + parents if @parent
      return parents
    end

    # see https://rdoc.info/gems/slim/frames#embedded-engines-markdown
    def generate_self_html
      html_help = {
        all_parents_ordered: self.get_all_parents_ordered,
      }
      @html = SlimRender.new.include_slim :details, { node: self, attributes: @attributes, html_help: html_help }
      self
    end

    # export html into a directory (if it exists)
    def output_html dir
      # puts "Write #{get_fullname} into #{dir}"
      file_path = dir + '/' + self.link
      puts "file_path #{file_path}"
      f = File.new(file_path, 'w+')
      f << @html
      f.close
      @childrens.each { |child| child.output_html dir }
    end

    FILTERS = {
      method: -> (node) { node.attributes['kind'] == 'function' && node.attributes['scope'] == 'instance' },
      static: -> (node) { node.attributes['kind'] == 'function' && node.attributes['scope'] == 'static' },
      default: -> (node) { !FILTERS.keys.reject { |fk| fk == :default }.any? { |filter| FILTERS[filter].call(node) } },
    }

    def childrens(filter: nil)
      filter ? @childrens.select do |child|
        FILTERS[filter].call(child)
      end : @childrens
    end
  end
end
