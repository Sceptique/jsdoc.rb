require 'json'

require './lib/node.rb'

module Jsdoc
  class Tree < Jsdoc::Node
    def self.load_file json_path
      puts "> json_path", json_path
      nodes = JSON.load_file json_path
      Tree.new nodes
    end

    def initialize(data_nodes)
      super(name: "", daddy: self)
      distribute_data_nodes data_nodes: data_nodes, filters: [
                              { "kind" => "class" },
                              { "kind" => "function" },
                              # { "kind" => "constant" },
                              # { "kind" => "member" },
                              # { "kind" => "package" },
                            ]
    end

    def distribute_data_nodes(data_nodes: nil, filters: [])
      selected_nodes = data_nodes.select do |data_node|
        filters.any? { |filter_group| filter_group.all? { |k, v| data_node[k] == v } }
      end

      selected_nodes.each do |data_node|
        self.add_data_node data_node
      end
    end
  end
end
