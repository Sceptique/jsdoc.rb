Gem::Specification.new do |s|
  s.name        = 'jsdoc.rb'
  s.version     = '0.0.0'
  s.summary     = "jsdoc like generator for javascript code"
  s.description = "An automatic documentation generator for javascript code"
  s.authors     = ["Arthur POULET"]
  s.email       = 'arthur.poulet@sceptique.eu'
  s.files       = ["lib/ooo.rb"]
  s.homepage    =
    'https://rubygems.org/gems/jsdoc.rb'
  s.license       = 'MIT'
end
