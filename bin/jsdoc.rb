#!/usr/bin/env ruby

require 'pry'

input_dir = ARGV[0]
output_dir = ARGV[1]
json_path = "#{output_dir}/ast.json"

`mkdir -p #{output_dir}`
`jsdoc #{input_dir} -r -X -d /dev/null > #{json_path}`

require './lib/tree.rb'

tree = Jsdoc::Tree.load_file json_path

pp tree
tree.generate_html_tree
tree.output_html output_dir
