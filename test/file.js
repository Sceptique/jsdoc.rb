/**
 * @memberof Grosns.Delamort
 * @class
 * @description Ceci est la classe personne
 *
 * @prop {String} name mon nom
 * @prop {Number} age
 * @prop {Grosns.Shared.Birthdate} birth devine
 */
class Person {
  /**
   * @constructor
   * @description constructeur de p
   * @param {String} name description of attr
   * @param {String} birthdate
   * @param {Object} named
   * @param {String} named.something descrit très bien
   */
  constructor() { }
}

/**
 * @class
 * @memberof Grosns.Shared
 * @description Ceci est la classe date de naissance
 *
 * @prop {Date} datetime
 */
class Birthdate {
  /**
   * @constructor
   * @description constructeur de b
   * @param {String} dt
   */
  constructor() { }

  /** */
  static superstatic() { }

  /** */
  supermember() { }
}

/**
 * @namespace Grosns
 *
 * @description C'est très important
 */
